define(['text!../protocols.html'], function(templ){
    var ProtocolsView = Backbone.View.extend({

        tagName: "div",
        id: "protocols-view-1",
        initialize: function() {
            $("#app_container").html(this.el);
            this.render();
        },

        render: function () {
            $(this.el).html(templ);
            return this;
        }
    });

    return ProtocolsView;
});
