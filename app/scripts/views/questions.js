define(['text!../questions.html', 'collections/question'], function(templ, QuestionCollection){

    var QuationsView = Backbone.View.extend({

        tagName: "div",
        id: "quations-view-1",
        initialize: function() {
            $("#app_container").html(this.el);
            this.render();

            var questions = new QuestionCollection ();
            questions.fetch ();
        },

        render: function () {
            $(this.el).html(templ);
            return this;
        }
    });

    return QuationsView;
});
