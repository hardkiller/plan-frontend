define(['text!../party.html'], function(templ){
    var PartyView = Backbone.View.extend({

        tagName: "div",
        id: "party-view-1",
        initialize: function() {
            $("#app_container").html(this.el);
            this.render();
        },

        render: function () {
            $(this.el).html(templ);
            return this;
        }
    });

    return PartyView;
});
