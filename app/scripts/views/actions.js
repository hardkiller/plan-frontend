define(['text!../actions.html'], function(templ){

    var actionsTempl = "";

    var ActionsView = Backbone.View.extend({

        tagName: "div",
        id: "discussions-view-1",
        initialize: function() {
            $("#app_container").html(this.el);
            this.render();
        },

        render: function () {
            $(this.el).html(templ);
            return this;
        }
    });

    return ActionsView;
});
