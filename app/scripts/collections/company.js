define(['models/Company', 'underscore'], function(CompanyModel, _){

    var CompanyCollection = Backbone.Collection.extend({

        model : CompanyModel,
        url : '/index.php?&ajax=true&c=object&a=list_objects', // ?context={%222%22:%5B0,3%5D,%223%22:%5B0%5D}

        initialize: function(){
            this.fetch({
                success: this.fetchSuccess,
                error: this.fetchError
            });
        },

        fetchSuccess: function (collection, response) {

            collection.each(function (item) {

                _.each (item.get ('objects'), function (ob) {

                    console.log ('object id: ' + ob.object_id + ' name : ' + ob.name);
                });

            });
        },

        fetchError: function (collection, response) {
            throw new Error("collection fetch error :|");
        }
    });

    return CompanyCollection;

});