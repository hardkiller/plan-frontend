define(['views/view1',
        'views/view2',
        'views/actions',
        'views/discussions',
        'views/quations',
        'views/protocols',
        'views/party'],
    function(View1,
             View2,
             ActionsView,
             DiscussionsView,
             QuationsView,
             ProtocolsView,
             PartyView){
  var TestRouter = Backbone.Router.extend({

      routes : {
          ""  : "rootview",
          "actions": "actions",
          "protocols": "protocols",
          "quations" : "quations",
          "discussions" : "discussions",
          "party": "party"
      },
      rootview: function () {
          alert ("RootView");
      },
      actions: function () {
          var ac = new ActionsView ();
      },
      protocols: function () {
          var pv = new ProtocolsView ();
      },
      quations: function () {
          var qv = new QuationsView ();
      },
      discussions: function () {
          var dv = new DiscussionsView ();
      },
      party: function () {
          var pv = new PartyView ();
      }
  });

  return TestRouter;
});
