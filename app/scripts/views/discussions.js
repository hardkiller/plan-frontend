define(['text!../all-talks.html'], function(templ){
    var DiscussionsView = Backbone.View.extend({

        tagName: "div",
        id: "discussions-view-1",
        initialize: function() {
            $("#app_container").html(this.el);
            this.render();
        },

        render: function () {
            $(this.el).html(templ);
            return this;
        }
    });

    return DiscussionsView;
});
