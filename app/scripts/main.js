/*global require*/
'use strict';

require.config({
    shim: {
        underscore: {
            exports: '_'
        },
        backbone: {
            deps: [
                'underscore',
                'jquery',
            ],
            exports: 'Backbone'
        },
        bootstrap: {
            deps: ['jquery'],
            exports: 'jquery'
        }
    },
    paths: {
        text: '../bower_components/requirejs-text/text',
        jquery: '../bower_components/jquery/jquery',
        backbone: '../bower_components/backbone/backbone',
        underscore: '../bower_components/underscore/underscore',
        bootstrap: 'vendor/bootstrap'
    }
});

require([
    'backbone',
    'text!./actions.html',
    'routers/test'
], function (Backbone, TextObj, MainRouter) {

    var PlanBbb = Backbone.View.extend({
        render: function () {
            // $(this.el).html("<h1>Hello world</h1>");
            return this;
        },

        initialize: function() {
            PlanBbb.Router = new MainRouter();
            Backbone.history.start();
        }
    });

    var planBbb = new PlanBbb();
    console.log('Hello from Backbone!');
});
